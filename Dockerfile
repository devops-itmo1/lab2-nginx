FROM alpine:latest AS build

ARG VERSION=1.23.3

ARG GPG_KEYSERVER=keyserver.ubuntu.com

ARG NGINX_GPG_SIGN_KEY

WORKDIR /tmp/

RUN apk --no-cache --update add                                                     \
        build-base                                                                  \
        gnupg                                                                       \
        pcre-dev                                                                    \
        wget                                                                        \
        zlib-dev                                                                    \
        linux-headers                                                               \
        libressl-dev                                                                \
        zlib-static


RUN set -x                                                                      &&  \
    gpg --keyserver "$GPG_KEYSERVER" --recv-keys "$NGINX_GPG_SIGN_KEY"          &&  \
    wget -q http://nginx.org/download/nginx-${VERSION}.tar.gz                   &&  \
    wget -q http://nginx.org/download/nginx-${VERSION}.tar.gz.asc               &&  \
    gpg --verify nginx-${VERSION}.tar.gz.asc                                    &&  \
    tar -xf nginx-${VERSION}.tar.gz                                             &&  \
    echo ${VERSION}

# Перейдем в директорию с исходным кодом NGINX
WORKDIR /tmp/nginx-${VERSION}

# Соберем и установим NGINX из исходного кода
RUN ./configure                                                                     \
        --with-ld-opt="-static"                                                     \
        --with-http_ssl_module                                                  &&  \
    make install                                                                &&  \
    strip /usr/local/nginx/sbin/nginx

FROM scratch

# Скопируем собранный бинарный файл NGINX из предыдущего образа, описанного выше
COPY --from=build /usr/local/nginx /usr/local/nginx
COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /var/lib /var/lib

# Скопируем с хоста конфигурацию для сервера
COPY nginx.conf /usr/local/nginx/conf/
COPY index.html /usr/share/nginx/html/

# Для того, чтобы обеспечить 'Graceful shutdown' поменяем посылаемый сигнал при выключении
# https://nginx.org/en/docs/control.html
STOPSIGNAL SIGQUIT

# Открываем порт по умолчанию
EXPOSE 80

# Объявим параметры запуска по умолчанию
ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]